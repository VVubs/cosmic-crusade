Cosmic Crusade is a 2 player twin-stick space shooter developed as a second year Game Development Workshop project.

Cosmic Crusade was created by ArcLight Studios, a team formed by Quinn Daggett, Matthew Demoe, Regan Tran, Aaron Macaulay, and Sawyer Shipp-Wiedersprecher.

2 Xbox controllers required in order to play.

Credits:
- Quinn Daggett: gameplay programming, sound design
- Matthew Demoe: gameplay programming, graphics programming
- Regan Tran: 3D modelling
- Aaron Macaulay: gameplay programming, enemy AI programming
- Sawyer Shipp-Wiedersprecher: Texture creation

[![Gameplay footage here](/uploads/539c4fc54334465cb2c47cb06dade001/Cosmic_crusade_image.png)](https://www.youtube.com/watch?v=sZ2gvdRBuDk)
